# GraphQL server with Node.js

GraphQL is a broad topic with its own query language, schema, operations, mutators, resolvers, etc. In this example are just some of the most basic examples of setting up a GraphQL server. You can read more about it in their [official documentation](https://graphql.org/learn/).

In this example, I've added the grpahql and express-grapql modules to an express app, defined some data, and defined a schema of that data. Then, I've written some simple oprations for the data and exposed those to the GraphQL library. This means, that I can now call those operations on that data through a GraphQL query.

## Installation & running

The usual stuff...

```
npm install

node index.js
```

Then open a browser window to ```http://localhost:3000/graphql```. What you should see is the GraphiQL editor window. This comes from the express-graphql module. You can use it to test queries on the API. You can also write a client application and make queries programmatically using HTTP.

## Notes

I've exposed the functionality to get all posts and authors, get a single post by its id, and get a single author based on their email address. In this picture, you can see some queries I've written for my API.

![GraphiQL Editor](graphiql.png)

The query getSinglePost is expecting a postID parameter of type Int. It is defined in the _query variables_ window. Similarly, the getAuthorByEmail is expecting a String variable by the name of authorEmail.

The query allAuthors is different, as it does not require any query variables.

Notice, how we can specify exactly what fields to include in the response, unlike with REST APIs.
