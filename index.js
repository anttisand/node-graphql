const express = require('express');

//graphqlHTTP will handle the client-server communication when using the /graphql endpoint
const express_graphql = require('express-graphql').graphqlHTTP;
//Graphql is strongly defined using a schema
const { buildSchema } = require('graphql');

const app = express();

app.use(express.json()); //Simplifies handling JSON
app.use(express.urlencoded({extended: true})); //Simplifies handling urlencoded string, for example, the query string

//This is our API data we would like to expose
const data = {
    posts: [
        { id: '001', title: 'Hello, GrpahQL', body: 'Lorem ipsum...' },
        { id: '002', title: 'Goodbye, GrpahQL', body: 'Dolores sit amet...' },
    ],
    authors: [
        { id: '001', name: 'John Doe', email: 'john.doe@example.com' },
        { id: '002', name: 'Jane Doe', email: 'jane.doe@example.com' },
    ]
};

//This schema defines the resources and field types
//for example, title: String! means, that the title value must be of type string and it cannot be empty (the ! -mark means it's required)
//Once we've defined our resources, Post and Author, we define the queries we allow and the parameters we allow for those queries
//post(id: Int!): Post will return a single instance of Post and requires an id of type Int
//posts: [Post] does not require any parameters and will return an array of Post resources
//authors(email: String!): Author means that we can query for a single author by their email address
const schema = buildSchema(`
type Post {
    id: ID!
    title: String!
    body: String!
}

type Author {
    id: ID!
    name: String!
    email: String!
}

type Query {
    post(id: Int!): Post
    posts: [Post]
    author(email: String!): Author
    authors:[Author]
}`);

//Here we define the internal logic of how we select the data to return
//getPost returns a single post by the id
const getPost = (args) => {
    const id = args.id;
    return data.posts.filter(post => {
        return post.id == id;
    })[0];
};

//getPosts returns all posts
const getPosts = (args) => {
    return data.posts;
}

//getAuthor reutrns a single author based on their email address
const getAuthor = (args) => {
    const email = args.email;
    return data.authors.filter(author => {
        return author.email == email;
    })[0];
}

//return all authors
const getAuthors = () => {
    return data.authors;
};

//Define the querys we allow the client to perform and the internal logic, that will satisfy these queries
const root = {
    post: getPost,
    posts: getPosts,
    author: getAuthor,
    authors: getAuthors,
};

app.use('/graphql', //graphql requests are made to /graphql endpoint
    express_graphql({ //have the graphql HTTP module handle the request for us
        schema: schema, //Define the schema, what data and fields we offer
        rootValue: root, //Define the queries we offer on that data
        graphiql: true, //enable graphiql, this is the graphql testing UI
    })
);

app.listen(3000, () => {
    console.log(`The server is listening at http://localhost:3000/graphql`);
});
